import React, { Component, Fragment } from 'react';
import './NoteList.css';

class NoteList extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.note.name !== this.props.note.name ||
            newProps.changeId !== this.props.changeId);
  }

  render() {
    return (
      <Fragment>
        <textarea
          type="text"
          className="NoteInput"
          value={this.props.note.name}
          onChange={(event) => this.props.change(event, this.props.note.id)}
        />
        <div className="NoteControl">
          {this.props.changeId === this.props.note.id ? (<p
            className="ItemSave"
            onClick={() => this.props.save(this.props.note.id)}
          >SAVE</p>) : null}
          <img
            src="img/ic-del.png"
            className="IconRemove"
            alt="Remove"
            title="Remove this note"
            onClick={() => this.props.remove(this.props.note.id)}
          />
        </div>
      </Fragment>
    );
  }
}

export default NoteList;