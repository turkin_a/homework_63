import React from 'react';
import './Modal.css';
import Button from "../Button/Button";

const Modal = props => {
  return <div className="Overlay">
    <div className="Modal">
      <div className="ModalBody">
        {props.children}
      </div>
      <Button onClick={() => this.props.clicked()}>SAVE</Button>
      <Button onClick={() => this.props.clicked()}>CANCEL</Button>
    </div>
  </div>;
};

export default Modal;