import React from 'react';
import './Loader.css';

const Loader = () => {
  return (
    <div className="Loader">
      <img src="img/preloader.svg" alt=""/>
    </div>
  );
}

export default Loader;