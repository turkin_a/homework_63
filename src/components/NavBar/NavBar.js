import React from 'react';
import './NavBar.css';
import {NavLink} from "react-router-dom";

const NavBar = () => {
  return (
    <ul className="Nav">
      <li><NavLink to="/todolist" className="Nav-Item" activeClassName="Selected">Todo list</NavLink></li>
      <li><NavLink to="/movielist" className="Nav-Item" activeClassName="Selected">Movie list</NavLink></li>
      <li><NavLink to="/notes" className="Nav-Item" activeClassName="Selected">Notes</NavLink></li>
    </ul>
  );
};

export default NavBar;