import React, { Component } from 'react';
import './InputNote.css';
import Button from "../UI/Button/Button";

class InputNote extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.value !== this.props.value);
  }

  render() {
    return (
      <div className="InputNote">
        <textarea type="text" value={this.props.value} onChange={(event) => this.props.change(event)}/>
        <Button click={this.props.click}>Add</Button>
      </div>
    );
  }
}

export default InputNote;