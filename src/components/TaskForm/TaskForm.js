import React from 'react';
import './TaskForm.css';
import Button from "../UI/Button/Button";

const TaskForm = props => {
  return (
    <div className="TaskForm">
      <p><input type="text" placeholder="Add new task" value={props.value} onChange={props.change} /></p>
      <Button click={props.add}>Add</Button>
    </div>
  );
};

export default TaskForm;