import React, { Component } from 'react';
import './InputMovie.css';
import Button from "../UI/Button/Button";

class InputMovie extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.value !== this.props.value);
  }

  render() {
    return (
      <div className="InputMovie">
        <input type="text" value={this.props.value} onChange={(event) => this.props.change(event)}/>
        <Button click={this.props.click}>Add</Button>
        <p>To watch list:</p>
      </div>
    );
  }
}

export default InputMovie;