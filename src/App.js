import React, { Component } from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";

import NavBar from "./components/NavBar/NavBar";
import TodoList from "./containers/TodoList/TodoList";
import MovieList from "./containers/MovieList/MovieList";
import Notes from "./containers/Notes/Notes";

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <Switch>
          <Route path="/" exact render={() => <h1>Select application</h1>} />
          <Route path="/todolist" component={TodoList} />
          <Route path="/movielist" component={MovieList} />
          <Route path="/notes" component={Notes} />
          <Route render={() => <h1>404: Page not found</h1>} />
        </Switch>
      </div>
    );
  }
}

export default App;
