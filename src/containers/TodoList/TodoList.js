import React, { Component } from 'react';
import './TodoList.css';

import axios from "../../axios-orders";

import Task from "../../components/Task/Task";
import TaskForm from "../../components/TaskForm/TaskForm";
import Loader from "../../components/UI/Loader/Loader";

class TodoList extends Component {
  state = {
    tasks: [],
    currentTask: '',
    loading: true
  };

  currentTask = (event) => {
    const currentTask = event.target.value;

    this.setState({currentTask});
  };

  addTask = () => {
    this.setState({loading: true});
    if (this.state.currentTask) {
      axios.post('/tasks.json', {info: this.state.currentTask, resolved: false}).finally(() => {
        this.loadTasks();
      });
    }
  };

  loadTasks = () => {
    axios.get('tasks.json').then((response) => {
      let tasks = [];
      for (let key in response.data) {
        tasks.push({id: key, info: response.data[key].info, resolved: response.data[key].resolved});
      }

      this.setState({tasks, currentTask: '', loading: false});
    }).catch(error => console.log(error));
  };

  deleteTask = (event, id) => {
    this.setState({loading: true});
    axios.delete(`/tasks/${id}.json`).finally(() => {
      this.loadTasks();
    });
  };

  markAtResolved = (event, id) => {
    const tasks = [...this.state.tasks];
    const index = this.state.tasks.findIndex(t => t.id === id);

    let task = {info: tasks[index].info, resolved: !tasks[index].resolved};

    axios.put(`/tasks/${id}.json`, task).finally(() => {
      this.loadTasks();
    });
  };

  componentDidMount() {
    this.loadTasks();
  }

  render() {
    let tasks = '';

    if (this.state.loading) tasks = <Loader />;
    else tasks = (
      <div>
        {
          this.state.tasks.map((task) => {
            return <Task
              classList={'TaskBox' + (task.resolved ? ' resolved' : '')}
              key={task.id}
              info={task.info}
              del={(event) => this.deleteTask(event, task.id)}
              resolved={(event) => this.markAtResolved(event, task.id)}
            />
          })
        }
      </div>
    );

    return (
      <div className="TodoList">
        <TaskForm
          change={(event) => this.currentTask(event)}
          add={this.addTask}
          value={this.state.currentTask}
        />
        <div className="Tasks">
          {tasks}
        </div>
      </div>
    );
  }
}

export default TodoList;