import React, { Component } from 'react';
import './Notes.css';
import InputNote from "../../components/InputNote/InputNote";
import NoteList from "../../components/NoteList/NoteList";
import axios from "../../axios-orders";
import Loader from "../../components/UI/Loader/Loader";

class Notes extends Component {
  state = {
    noteList: [],
    newNote: '',
    loading: true,
    changeId: ''
  };

  handleNewNote = (event) => {
    const newNote = event.target.value;

    this.setState({newNote});
  };

  handleChangeNote = (event, id) => {
    const index = this.state.noteList.findIndex(n => n.id === id);
    const noteList = [...this.state.noteList];
    const changingNote = {...noteList[index]};
    changingNote.name = event.target.value;
    noteList[index] = changingNote;

    this.setState({noteList, changeId: id});
  };

  handleSaveChange = (id) => {
    this.setState({loading: true});
    const index = this.state.noteList.findIndex(n => n.id === id);
    let note = {name: this.state.noteList[index].name};

    axios.put(`/notes/${id}.json`, note).finally(() => {
      this.loadNotes();
    });
  };

  addNote = () => {
    if (this.state.newNote === '') return null;
    this.setState({loading: true});

    axios.post('/notes.json', {name: this.state.newNote}).finally(() => {
      this.loadNotes();
    });
  };

  loadNotes = () => {
    axios.get('notes.json').then((response) => {
      let noteList = [];
      for (let key in response.data) {
        noteList.push({id: key, name: response.data[key].name});
      }

      this.setState({noteList, newNote: '', loading: false, changeId: ''});
    }).catch(error => console.log(error));
  };

  removeNote = (id) => {
    this.setState({loading: true});
    axios.delete(`/notes/${id}.json`).finally(() => {
      this.loadNotes();
    });
  };

  componentDidMount() {
    this.loadNotes();
  }

  render() {
    let noteList = '';

    if (this.state.loading) noteList = <Loader />;
    else noteList = (
      <div className="NoteList">
        {this.state.noteList.map(note => {
          return (
            <NoteList
              key={note.id}
              note={note}
              change={this.handleChangeNote}
              remove={this.removeNote}
              save={this.handleSaveChange}
              changeId={this.state.changeId}
            />
          );
        })}
      </div>
    );
    return (
      <div className="Notes">
        <InputNote
          value={this.state.newNote}
          change={this.handleNewNote}
          click={this.addNote}
        />
        {noteList}
      </div>
    );
  }
}

export default Notes;