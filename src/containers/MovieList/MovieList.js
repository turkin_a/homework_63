import React, { Component } from 'react';
import './MovieList.css';

import axios from '../../axios-orders';

import InputMovie from "../../components/InputMovie/InputMovie";
import WatchList from "../../components/WatchList/WatchList";
import Loader from "../../components/UI/Loader/Loader";

class MovieList extends Component {
  state = {
    movieList: [],
    newMovie: '',
    loading: true,
    changeId: ''
  };

  handleNewMovie = (event) => {
    const newMovie = event.target.value;

    this.setState({newMovie});
  };

  handleChangeMovie = (event, id) => {
    const index = this.state.movieList.findIndex(m => m.id === id);
    const movieList = [...this.state.movieList];
    const changingMovie = {...movieList[index]};
    changingMovie.name = event.target.value;
    movieList[index] = changingMovie;

    this.setState({movieList, changeId: id});
  };

  handleSaveChange = (id) => {
    this.setState({loading: true});
    const index = this.state.movieList.findIndex(m => m.id === id);
    let movie = {name: this.state.movieList[index].name};

    axios.put(`/movies/${id}.json`, movie).finally(() => {
      this.loadMovies();
    });
  };

  addMovie = () => {
    if (this.state.newMovie === '') return null;
    this.setState({loading: true});

    axios.post('/movies.json', {name: this.state.newMovie}).finally(() => {
      this.loadMovies();
    });
  };

  loadMovies = () => {
    axios.get('movies.json').then((response) => {
      let movieList = [];
      for (let key in response.data) {
        movieList.push({id: key, name: response.data[key].name});
      }

      this.setState({movieList, newMovie: '', loading: false, changeId: ''});
    }).catch(error => console.log(error));
  };

  removeMovie = (id) => {
    this.setState({loading: true});
    axios.delete(`/movies/${id}.json`).finally(() => {
      this.loadMovies();
    });
  };

  componentDidMount() {
    this.loadMovies();
  }

  render() {
    let watchList = '';

    if (this.state.loading) watchList = <Loader />;
    else watchList = (
      <div className="WatchList">
        {this.state.movieList.map(movie => {
          return (
            <WatchList
              key={movie.id}
              movie={movie}
              change={this.handleChangeMovie}
              remove={this.removeMovie}
              save={this.handleSaveChange}
              changeId={this.state.changeId}
            />
          );
        })}
      </div>
    );

    return (
      <div className="MovieList">
        <InputMovie
          value={this.state.newMovie}
          change={this.handleNewMovie}
          click={this.addMovie}
        />
        {watchList}
      </div>
    );
  }
}

export default MovieList;