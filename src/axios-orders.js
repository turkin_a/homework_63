import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://notes-fulion.firebaseio.com/'
});

export default instance;